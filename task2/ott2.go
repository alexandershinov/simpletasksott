package main

import (
	"fmt"
	"math"
	"math/rand"
	"bytes"
	"time"
	"errors"
)

func SpiralOutput(m []int) string {
	var result bytes.Buffer
	n := int(math.Sqrt(float64(len(m)))) / 2
	if len(m) != (n*2+1)*(n*2+1) {
		panic(errors.New("Array size error."))
	}
	i := []int{n, n};
	// dimension -1 - horizontal, 1 - vertical
	dimension := 1
	// direction 1 - positive, -1 - negative
	direction := -1
	// l - 2 * length of current direct path
	for l := 2; i[1] >= 0; l++ {
		// Change dimension every step
		dimension *= -1
		// Change direction only if dimension is vertical
		if dimension > 0 {
			direction *= -1
		}
		// Direct path
		for j := 0; j < l/2; j++ {
			// Write current last step value to result
			fmt.Fprintf(&result, "%d ", m[i[0]*(2*n+1)+i[1]])
			// Do step
			i[(1-dimension)/2] += direction
		}
	}
	return result.String()
}

func main() {
	rands := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(rands)
	m := make([]int, 25)
	for i := range m {
		m[i] = r1.Intn(100)
	}
	fmt.Println("in:")
	l := int(math.Sqrt(float64(len(m))))
	for i := 0; i < len(m); i+=l {
		fmt.Println(m[i:i+l])
	}
	fmt.Println("out:")
	fmt.Println(SpiralOutput(m))
}
