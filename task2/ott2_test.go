package main

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

type spiralTest struct {
	Data []int
	Result string
}

func (test *spiralTest) Do(t *testing.T) {
	assert.Equal(t, test.Result, SpiralOutput(test.Data))
}

func TestSpiralOutput(t *testing.T) {
	for name, test := range map[string]spiralTest{
		"3x3": {
			Data:[]int{1,2,3,4,5,6,7,8,9},
			Result:"5 4 7 8 9 6 3 2 1 ",
		},
		"5x5": {
			Data:[]int{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25},
			Result:"13 12 17 18 19 14 9 8 7 6 11 16 21 22 23 24 25 20 15 10 5 4 3 2 1 ",
		},
		"1x1": {
			Data:[]int{1},
			Result:"1 ",
		},
	} {
		t.Run(name, test.Do)
	}
}
