package main

import (
	"fmt"
	"strings"
	"bytes"
	"os"
	"io"
	"flag"
	"errors"
)

func task1(S *string, input io.Reader) (m uint64, coordinates []*uint64, err error) {
	// For compare runes easier use ToLower method
	*S = strings.ToLower(*S)
	// coordinates is an list (with the same length as *S) of chars compressed coordinates in input "array"
	coordinates = make([]*uint64, len(*S))
	var n uint64
	// Read array dimensions from input
	_, err = fmt.Fscanf(input, "%d %d\n", &n, &m)
	if err != nil {
		err = errors.New("Impossible")
		return
	}
	// a is one line of array as type string
	var a string
	// i is current line number (first coordinate)
	var i uint64
	for i = 0; i < n; i++ {
		// Scan line
		_, err =fmt.Fscanln(input, &a)
		if err != nil || len(a) != int(m) {
			err = errors.New("Impossible")
			return
		}
		// For every rune in line find the same (free) rune in *S
		// Free means nil pointer in coordinates list (for this rune position in *S)
		for j, w := range strings.ToLower(a) {
			for k, v := range *S {
				if w == v && coordinates[k] == nil {
					coordinates[k] = new(uint64)
					*coordinates[k] = i*m + uint64(j)
					*S = fmt.Sprintf("%s%c%s", (*S)[:k], a[j], (*S)[k+1:])
					break
				}
			}
		}
	}
	return
}

func main() {
	// Get parameters from flags
	// Usage >> go run main.go --in=input.txt --out=output.txt
	inputFile := flag.String("in", "", "Input file name. If not exists, stdin uses. Example: go run main.go -in=input.txt")
	outputFile := flag.String("out", "", "Output file name. If not exists, outdin uses. Example: go run main.go -out=output.txt")
	flag.Parse()
	// Open input and output files
	input, err := os.Open(*inputFile)
	if err != nil {
		panic(err)
	}
	defer input.Close()
	os.Remove(*outputFile)
	output, err := os.OpenFile(*outputFile, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}
	defer output.Close()
	// Algorithm start
	// Firstly, create new variable with phrase to find
	S := "OneTwoTrip"
	// Execute function task1 (main part of algorithm)
	m, taskResult, err := task1(&S, input)
	if err != nil {
		fmt.Fprint(output, err.Error())
		return
	}
	// Write results to output file
	var result bytes.Buffer
	for i, w := range S {
		// If any rune wasn`t found in input file, return Impossible
		if taskResult[i] == nil {
			err = errors.New("Impossible")
			fmt.Fprint(output, err.Error())
			return
		}
		fmt.Fprintf(&result, "%c - (%d, %d)\n", w, *taskResult[i]/m, *taskResult[i]%m)
	}
	// If everything is OK, write result to output
	fmt.Fprint(output, result.String())
}
